User.delete_all
Twitt.delete_all

10.times do 
  User.create  name: Faker::Name.first_name ,
               surname: Faker::Name.last_name,
               email: Faker::Internet.email,
               password: Faker::Internet.password(6),
               photo: Faker::Internet.url,
               info: Faker::Lorem.sentence(3)
              
end
100.times do
  t = Twitt.new(text: Faker::Lorem.sentence(4))
  t.users << User.order("Random()").limit(1) 
  t.save
end
