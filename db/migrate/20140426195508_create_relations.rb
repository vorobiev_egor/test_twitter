class CreateRelations < ActiveRecord::Migration
  def change
    create_table :relations do |t|
      t.integer :user_id
      t.integer :follower_id
      t.integer :subscription_id

      t.timestamps
    end
  end
end
