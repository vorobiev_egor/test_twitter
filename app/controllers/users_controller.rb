class UsersController < ApplicationController
  before_action :logged_in,only: [:edit,:update,:destroy]
  before_action :presence_id,only: [:show]

  def start
    if current_user
      redirect_to current_user
    else
      redirect_to new_user_path
    end
  end

  def show
    @twitt = Twitt.new
    @user = User.find_by(id:  params[:id] || cur_id)
  end

  def new
    @user = User.new
  end

  def create
    @user = User.new(sign_in_params)
    if @user.save
      set_id @user.id
      redirect_to @user
    else
      render 'new'
    end
  end

  def sign_up
    @user = User.new
  end

  def enter
    if @user = User.find_by(sign_up_params)
      set_id @user.id    
      redirect_to @user
    else
      render 'sign_up'
    end
  end

  def edit
    @user = current_user 
  end

  def update
    @user = current_user
    if @user.update_attributes(sign_in_params)
      redirect_to @user 
    else
      render 'edit'
    end
  end

  def log_out
    reset_id
    binding.pry
    redirect_to sign_up_users_path
  end

  def destroy
    @user = current_user
    @user.destroy
    reset_id
    redirect_to sign_in_users_path
  end

  private

   def presence_id
      unless  cur_id || params[:id]
        redirect_to sign_up_users_path
      end
   end

   def set_id(id)
     session[:current_user_id] = id
   end
  
   def reset_id
     session[:current_user_id] = nil
   end
   
   def logged_in
     if cur_id != params[:id].to_i
        redirect_to :root
     end
   end
   
   def sign_up_params
     params.require(:user).permit(:login,:password)
   end

   def sign_in_params
     params.require(:user).permit(:login,:password,:email,:name,:surname,:photo,:info)
   end
end
