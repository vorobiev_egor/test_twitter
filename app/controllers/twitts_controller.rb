class TwittsController < ApplicationController

  def create
    @twitt = Twitt.new(twitt_params)
    if @twitt.save
      current_user.twitts << @twitt
      redirect_to current_user
    else
      render 'show'
    end
  end

  def destroy
    Twitt.find_by(id: params[:id]).destroy
    redirect_to current_user
  end

  def retwitt
    current_user.twitts << Twitt.find_by(id: params[:id]) 
    redirect_to current_user
  end

  private
    def twitt_params
      hash = params.require(:twitt).permit(:text)
      hash[:owner_id] = current_user.id
      hash
    end
end
