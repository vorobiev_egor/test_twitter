class User < ActiveRecord::Base
  validates :name,presence: true

  has_many :twitts,through: :dependences
  has_many :dependences

  
  has_many :subscriptions,through: :relations
  has_many :followers,through: :relations
  has_many :relations

end
