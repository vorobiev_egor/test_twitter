class Relation < ActiveRecord::Base
  belongs_to :subscription,class_name: User
  belongs_to :follower,class_name: User
end
